import os

import cv2
import keras_ocr

from addictional_tools.image import inpaint_text
from addictional_tools.logo import insert_image_above
from main import create_directory
import traceback

if __name__ == "__main__":
    removed_logo_images_folder = 'removed_images'
    new_logo_images_folder = 'new_images'
    images_list = list(os.listdir('product_images'))
    create_directory(os.getcwd(), removed_logo_images_folder)
    create_directory(os.getcwd(), new_logo_images_folder)
    print(images_list)
    for file_name in images_list:
        try:
            pipeline = keras_ocr.pipeline.Pipeline()
            img_text_removed = inpaint_text(f'product_images/{file_name}', pipeline)
            cv2.imwrite(f'removed_images/{file_name.split(".")[0]}_removed.jpg', cv2.cvtColor(img_text_removed, cv2.COLOR_BGR2RGB))

            base_image_path = f'removed_images/{file_name.split(".")[0]}_removed.jpg'
            image_to_insert_path = 'image001.png'
            output_image_path = f'new_images/output_{file_name.split(".")[0]}.jpg'
            max_width = 300
            max_height = 200

            insert_image_above(base_image_path, image_to_insert_path, output_image_path, max_width, max_height)
        except Exception as e:
            print(traceback.print_exc())

