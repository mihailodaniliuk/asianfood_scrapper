import os
import re
import time

import requests
import wget
from bs4 import BeautifulSoup
import lxml
import pandas as pd
import openpyxl
from seleniumbase import Driver
from config import LOGIN, PASSWORD
from openpyxl import Workbook
from openpyxl.drawing.image import Image as ExcelImage
import warnings
warnings.filterwarnings('ignore')

if __name__ == '__main__':
    images_folder_path = 'new_images'
    dataframe = pd.read_excel('Asian_food_temp.xlsx')
    wb = Workbook()
    ws = wb.active

    for col_index, col_name in enumerate(dataframe.columns, start=1):
        ws.cell(row=1, column=col_index, value=col_name)

    for index, row in dataframe.iterrows():
        for col_index, value in enumerate(row):
            ws.cell(row=index + 2, column=col_index + 1, value=value)

    for df_index, row in dataframe.iterrows():
        img_path = f"{images_folder_path}/output_{row['image'].replace(row['image'].split('.')[1],'jpg')}"
        img = ExcelImage(img_path)
        img.width = img.height = 50
        ws.add_image(img, f'O{df_index + 2}')
        ws.row_dimensions[df_index + 2].height = img.height / 1.25

    ws.delete_cols(1)

    wb.save('Asian_food.xlsx')