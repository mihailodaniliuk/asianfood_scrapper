import os
import re
import time

import requests
import wget
from bs4 import BeautifulSoup
import lxml
import pandas as pd
import openpyxl
from seleniumbase import Driver
from config import LOGIN, PASSWORD
from openpyxl import Workbook
from openpyxl.drawing.image import Image as ExcelImage
import warnings
warnings.filterwarnings('ignore')

producer_regex = '(^3{0,1}[A-Z\W]+)[ A-Z]{2}'
extension_regex = '[\S]+\.([a-z]+)$'

def create_directory(path_to_dir, name):
    mypath = f'{path_to_dir}/{name}'
    if not os.path.isdir(mypath):
        os.makedirs(mypath)


def get_all_products_by_category_link(category_url):
    product_links = []
    page = 1
    while True:
        start_len = len(product_links)
        response = requests.get(f'{category_url}?pageID={page}')
        soup = BeautifulSoup(response.text, 'lxml')
        part_product_links = [f"http://www.asianfood.no/{div.find('a').get('href')}" for div in
                              soup.find_all('div', class_='WebPubElement pub-productlisting')]
        product_links.extend([product_link for product_link in part_product_links if product_link not in product_links])
        page += 1
        # print(len(product_links))
        if start_len == len(product_links):
            break
        if page==1:
            break
    return product_links


def download_image(image_url, image_path):
    while True:
        try:
            if os.path.isfile(image_path):
                break
            wget.download(image_url, image_path)
            break
        except Exception as e:
            print(e)


def get_product_data(html_text):
    product_name, vietnamese_name, item_number, alt_item_number, image_url = '', '', '', '', ''
    price, old_price, discount, producer, expiration_date, category = '', '', '', '', '', ''
    wholesale_price_1, wholesale_price_2, wholesale_price_3 = '', '', ''
    soup = BeautifulSoup(html_text, 'lxml')
    data_block = soup.find('div', class_='product-info-container')
    product_name = data_block.find('h1').text
    vietnamese_name = data_block.find('h2').text
    try:
        item_number = data_block.find('div', class_='product-number').find('div',
                                                                           class_='product-number-inner').text.replace(
            'Varenr: ', '')
    except:
        pass
    try:
        alt_item_number = data_block.find('div', class_='product-number').find('div',
                                                                               class_='alt-product-number').text.replace(
            'Alt. varenr: ', '')
    except:
        pass
    try:

        image_url = 'http://www.asianfood.no' + soup.find('div',class_='main-container').find('div', class_='rsSlide').find('img').get('data-rsbigimg')
    except:
        pass
    try:
        price = soup.find('span', class_='PriceLabel has-discount product-price-api').text.strip().replace(',','.')
    except:
        pass
    try:
        price = soup.find('span', class_='PriceLabel product-price-api').text.strip().replace(',','.')
    except:
        pass
    try:
        old_price = soup.find('span', class_='OldPriceLabel').text.strip().replace(',','.')
    except:
        pass
    try:
        discounts_block = soup.find('div', id='quantity-discount-container').find('div',
                                                                                  class_='col-xs-12 quantity-discounts').find(
            'table').find('tbody')
        trs = discounts_block.find_all('tr')
        if len(trs) == 3:
            wholesale_price_1 =  trs[0].find_all('td')[
                1].text.strip().replace(',','.')
            wholesale_price_2 =  trs[1].find_all('td')[
                1].text.strip().replace(',','.')
            wholesale_price_3 =  trs[2].find_all('td')[
                1].text.strip().replace(',','.')
        elif len(trs) == 2:
            wholesale_price_1 = trs[0].find_all('td')[
                1].text.strip().replace(',','.')
            wholesale_price_2 =  trs[1].find_all('td')[
                1].text.strip().replace(',','.')
        elif len(trs) == 1:
            wholesale_price_1 =  trs[0].find_all('td')[
                1].text.strip().replace(',','.')
    except Exception as e:
        print(e)
    try:
        producer = re.findall(producer_regex, product_name)[0]
    except:
        pass
    try:
        category_block = soup.find('div', class_='BreadCrumb')
        category = ' '.join([f'{a.text} > ' for a in category_block.find_all('a')[1:-1]])
    except:
        pass
    try:
        expiration_date = soup.find('div', class_='expire-date-container').text.replace(
            'Holdbarhetsdato (webshop lager): ', '').strip()
    except:
        pass
    if price and old_price:
        try:
            discount = round(100 - (float(price.replace(',', '.').replace(' ', '')) / float(
                old_price.replace(',', '.').replace(' ', '')) * 100))
        except:
            pass
    image_extension = re.findall(extension_regex, image_url)[0]
    print({'category': category, 'product_name': product_name, 'producer': producer, 'vietnamese_name': vietnamese_name,
           'item_number': item_number,
           'alt_item_number': alt_item_number, 'price': price, 'old_price': old_price,
           'expiration_date': expiration_date,
           'image': f'{item_number}.{image_extension}', 'discount': discount, 'img': ''})
    # print(image_url)
    # time.sleep(10000)

    download_image(image_url, f'{images_folder_path}/{item_number}.{image_extension}')
    # print(image_url)
    return {'category': category, 'product_name': product_name, 'producer': producer,
            'vietnamese_name': vietnamese_name,
            'item_number': item_number,
            'alt_item_number': alt_item_number, 'price': price, 'old_price': old_price,
            'expiration_date': expiration_date, '2 - 4 Kartong': wholesale_price_1,
            '5 - 9 Kartong': wholesale_price_2, '10 +': wholesale_price_3,
            'image': f'{item_number}.{image_extension}', 'discounts': discount, 'img': ''}


def login():
    driver.get('http://www.asianfood.no/')
    time.sleep(2)
    driver.click('#loginout')
    time.sleep(2)
    driver.find_element('#username').send_keys(LOGIN)
    time.sleep(2)
    driver.find_element('#password').send_keys(PASSWORD)
    time.sleep(2)
    driver.click('#login > div.col-sm-7.login-left > div.form-horizontal > div.form-group.login-g-btn > div > button')
    driver.wait_for_element('#o-wrapper', timeout=10)


def get_category_links(html_text):
    soup = BeautifulSoup(html_text, 'lxml')
    nav_block = soup.find('ul', class_='nav navbar-nav')
    category_urls = [f'http://www.asianfood.no{a.get("href")}' for a in
                     nav_block.find_all('a', class_='dropdown-toggle')]
    return category_urls


def scrape():
    driver.set_page_load_timeout(40)
    driver.maximize_window()
    while True:
        try:
            driver.get('https://google.com')
            break
        except Exception as e:
            print(e)


    time.sleep(2)
    login()
    df_counter = 0

    dataframe = pd.DataFrame(
        columns=['category', 'product_name', 'producer', 'vietnamese_name',
                 'item_number', 'alt_item_number', 'price', 'old_price',
                 'expiration_date', 'image', 'discounts', '2 - 4 Kartong', '5 - 9 Kartong', '10 +',
                 'img', 'link'])
    # writer = pd.ExcelWriter('Asian_food.xlsx', engine='xlsxwriter')
    category_urls = get_category_links(driver.get_page_source())
    product_links = []
    for category_index, category_url in list(enumerate(category_urls)):
        product_links.extend(get_all_products_by_category_link(category_url))
        print(len(product_links))
    for product_index, link in list(enumerate(product_links)):
        print(link)
        driver.get(link)
        time.sleep(1)
        product_data = get_product_data(driver.get_page_source())
        product_data['link'] = link
        dataframe.loc[df_counter] = product_data
        dataframe.to_excel('Asian_food_temp.xlsx' ,engine='openpyxl')
        df_counter += 1



if __name__ == '__main__':
    images_folder_name = 'product_images'
    images_folder_path = f'{os.getcwd()}/{images_folder_name}'
    print(images_folder_path)
    driver = Driver(
        uc=True,
        headless2=True,
        # headed = True
    )
    create_directory(os.getcwd(),images_folder_name)
    scrape()
