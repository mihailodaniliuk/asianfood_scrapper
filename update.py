import os
import re
import time

import requests
import wget
from bs4 import BeautifulSoup
import lxml
import pandas as pd
import openpyxl
from seleniumbase import Driver
from config import LOGIN, PASSWORD
from openpyxl import Workbook
from openpyxl.drawing.image import Image as ExcelImage
import warnings
warnings.filterwarnings('ignore')

producer_regex = '(^3{0,1}[A-Z\W]+)[ A-Z]{2}'
extension_regex = '[\S]+\.([a-z]+)$'

def create_directory(path_to_dir, name):
    mypath = f'{path_to_dir}/{name}'
    if not os.path.isdir(mypath):
        os.makedirs(mypath)


def get_product_data(html_text):
    product_name, vietnamese_name, item_number, alt_item_number, image_url = '', '', '', '', ''
    price, old_price, discount, producer, expiration_date, category = '', '', '', '', '', ''
    wholesale_price_1, wholesale_price_2, wholesale_price_3 = '', '', ''
    soup = BeautifulSoup(html_text, 'lxml')
    data_block = soup.find('div', class_='product-info-container')
    product_name = data_block.find('h1').text
    vietnamese_name = data_block.find('h2').text
    try:
        item_number = data_block.find('div', class_='product-number').find('div',
                                                                           class_='product-number-inner').text.replace(
            'Varenr: ', '')
    except:
        pass
    try:
        alt_item_number = data_block.find('div', class_='product-number').find('div',
                                                                               class_='alt-product-number').text.replace(
            'Alt. varenr: ', '')
    except:
        pass
    try:

        image_url = 'http://www.asianfood.no' + soup.find('div',class_='main-container').find('div', class_='rsSlide').find('img').get('data-rsbigimg')
    except:
        pass
    try:
        price = soup.find('span', class_='PriceLabel has-discount product-price-api').text.strip()
    except:
        pass
    try:
        price = soup.find('span', class_='PriceLabel product-price-api').text.strip()
    except:
        pass
    try:
        old_price = soup.find('span', class_='OldPriceLabel').text.strip()
    except:
        pass
    try:
        discounts_block = soup.find('div', id='quantity-discount-container').find('div',
                                                                                  class_='col-xs-12 quantity-discounts').find(
            'table').find('tbody')
        trs = discounts_block.find_all('tr')
        if len(trs) == 3:
            wholesale_price_1 =  trs[0].find_all('td')[
                1].text.strip()
            wholesale_price_2 =  trs[1].find_all('td')[
                1].text.strip()
            wholesale_price_3 =  trs[2].find_all('td')[
                1].text.strip()
        elif len(trs) == 2:
            wholesale_price_1 = trs[0].find_all('td')[
                1].text.strip()
            wholesale_price_2 =  trs[1].find_all('td')[
                1].text.strip()
        elif len(trs) == 1:
            wholesale_price_1 =  trs[0].find_all('td')[
                1].text.strip()
    except Exception as e:
        print(e)
    try:
        producer = re.findall(producer_regex, product_name)[0]
    except:
        pass
    try:
        category_block = soup.find('div', class_='BreadCrumb')
        category = ' '.join([f'{a.text} > ' for a in category_block.find_all('a')[1:-1]])
    except:
        pass
    try:
        expiration_date = soup.find('div', class_='expire-date-container').text.replace(
            'Holdbarhetsdato (webshop lager): ', '').strip()
    except:
        pass
    if price and old_price:
        try:
            discount = round(100 - (float(price.replace(',', '.').replace(' ', '')) / float(
                old_price.replace(',', '.').replace(' ', '')) * 100))
        except:
            pass
    image_extension = re.findall(extension_regex, image_url)[0]
    print({'category': category, 'product_name': product_name, 'producer': producer, 'vietnamese_name': vietnamese_name,
           'item_number': item_number,
           'alt_item_number': alt_item_number, 'price': price, 'old_price': old_price,
           'expiration_date': expiration_date,
           'image': f'{item_number}.{image_extension}', 'discount': discount, 'img': ''})


    return {'category': category, 'product_name': product_name, 'producer': producer,
            'vietnamese_name': vietnamese_name,
            'item_number': item_number,
            'alt_item_number': alt_item_number, 'price': price, 'old_price': old_price,
            'expiration_date': expiration_date, '2 - 4 Kartong': wholesale_price_1,
            '5 - 9 Kartong': wholesale_price_2, '10 +': wholesale_price_3,
            'image': f'{item_number}.{image_extension}', 'discounts': discount, 'img': ''}


def login():
    driver.get('http://www.asianfood.no/')
    time.sleep(2)
    driver.click('#loginout')
    time.sleep(2)
    driver.find_element('#username').send_keys(LOGIN)
    time.sleep(2)
    driver.find_element('#password').send_keys(PASSWORD)
    time.sleep(2)
    driver.click('#login > div.col-sm-7.login-left > div.form-horizontal > div.form-group.login-g-btn > div > button')
    driver.wait_for_element('#o-wrapper', timeout=10)


def update():
    driver.set_page_load_timeout(40)
    driver.maximize_window()
    # while True:
    #     try:
    #         driver.get('https://google.com')
    #         break
    #     except Exception as e:
    #         print(e)


    time.sleep(2)
    login()
    df_counter = 0

    initial_dataframe = pd.read_excel('Asian_food_temp.xlsx')
    product_links = initial_dataframe['link']

    dataframe = pd.DataFrame(
        columns=['category', 'product_name', 'producer', 'vietnamese_name',
                 'item_number', 'alt_item_number', 'price', 'old_price',
                 'expiration_date', 'image', 'discounts', '2 - 4 Kartong', '5 - 9 Kartong', '10 +',
                 'img', 'link','previous price', 'previous old_price', 'previous discounts', 'previous 2 - 4 Kartong',
                 'previous 5 - 9 Kartong', 'previous 10 +','price changing','old price changing','discounts changing',
                 '2 - 4 Kartong changing','5 - 9 Kartong changing','10 + changing'])

    time.sleep(10)
    for product_index, link in list(enumerate(product_links)):
        print(link)
        driver.get(link)
        time.sleep(1)
        product_data = get_product_data(driver.get_page_source())
        product_data['previous price'] = initial_dataframe.loc[product_index,'price']
        product_data['previous old_price'] = initial_dataframe.loc[product_index,'old_price']
        product_data['previous discounts'] = initial_dataframe.loc[product_index,'discounts']
        product_data['previous 2 - 4 Kartong'] = initial_dataframe.loc[product_index,'2 - 4 Kartong']
        product_data['previous 5 - 9 Kartong'] = initial_dataframe.loc[product_index,'5 - 9 Kartong']
        product_data['previous 10 +'] = initial_dataframe.loc[product_index,'10 +']
        product_data['link'] = initial_dataframe.loc[product_index, 'link']
        product_data['img'] = initial_dataframe.loc[product_index, 'img']
        product_data['price changing'] = float(product_data['price'].replace(',','.')) - float(product_data['previous price']) if product_data['price'] and product_data['previous price'] else ''
        product_data['old price changing'] = float(product_data['old_price'].replace(',','.')) - float(product_data['previous old_price']) if product_data['old_price'] and product_data['previous old_price'] else ''
        product_data['discounts changing'] = float(str(product_data['discounts']).replace(',','.')) - float(product_data['previous discounts']) if product_data['discounts'] and product_data['previous discounts'] else ''
        product_data['2 - 4 Kartong changing'] = float(product_data['2 - 4 Kartong'].replace(',','.')) - float(product_data['previous 2 - 4 Kartong']) if product_data['2 - 4 Kartong'] and product_data['previous 2 - 4 Kartong'] else ''
        product_data['5 - 9 Kartong changing'] = float(product_data['5 - 9 Kartong'].replace(',','.')) - float(product_data['previous 5 - 9 Kartong']) if product_data['5 - 9 Kartong'] and product_data['previous 5 - 9 Kartong'] else ''
        product_data['10 + changing'] = float(product_data['10 +'].replace(',','.')) - float(product_data['previous 10 +']) if product_data['10 +'] and product_data['previous 10 +'] else ''
        dataframe.loc[df_counter] = product_data
        dataframe.to_excel('Asian_food_final.xlsx' ,engine='openpyxl')
        df_counter += 1

    images_folder_path = 'new_images'
    dataframe = pd.read_excel('Asian_food_final.xlsx')
    wb = Workbook()
    ws = wb.active

    for col_index, col_name in enumerate(dataframe.columns, start=1):
        ws.cell(row=1, column=col_index, value=col_name)

    for index, row in dataframe.iterrows():
        for col_index, value in enumerate(row):
            ws.cell(row=index + 2, column=col_index + 1, value=value)

    for df_index, row in dataframe.iterrows():
        img_path = f"{images_folder_path}/output_{row['image'].replace(row['image'].split('.')[1], 'jpg')}"
        img = ExcelImage(img_path)
        img.width = img.height = 50
        ws.add_image(img, f'O{df_index + 2}')
        ws.row_dimensions[df_index + 2].height = img.height / 1.25

    ws.delete_cols(1)

    wb.save('Asian_food.xlsx')



if __name__ == '__main__':
    images_folder_name = 'product_images'
    images_folder_path = f'{os.getcwd()}/{images_folder_name}'
    print(images_folder_path)
    driver = Driver(
        uc=True,
        # headless2=True,
        headed = True
    )
    update()
